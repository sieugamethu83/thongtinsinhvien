﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLiHocSinh
{
    public partial class ThongTinHocVien : Form
    {
        private DangNhap dangNhap;
        private Form Form1;
        public ThongTinHocVien(Form Form1) {
            InitializeComponent();
            this.Form1 = Form1;
        }


        public ThongTinHocVien(DangNhap dangNhap)
        {
            this.dangNhap = dangNhap;
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void ThongTinHocVien_FormClosing(object sender, FormClosingEventArgs e)
        {
            Form1.Close();
        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            Form1.Close();
        }
    } 

}
