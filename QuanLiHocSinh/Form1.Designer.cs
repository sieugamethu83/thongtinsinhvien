﻿namespace QuanLiHocSinh
{
    partial class ThongTinHocVien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_ho_ten = new System.Windows.Forms.TextBox();
            this.textBox_so_cm = new System.Windows.Forms.TextBox();
            this.richTextBox_dia_chi = new System.Windows.Forms.RichTextBox();
            this.listBox_nam_sinh = new System.Windows.Forms.ListBox();
            this.comboBox_khoa_hoc = new System.Windows.Forms.ComboBox();
            this.groupBox_gioi_tinh = new System.Windows.Forms.GroupBox();
            this.radioButton_nu = new System.Windows.Forms.RadioButton();
            this.radioButton_nam = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.listBox_thoi_gian_hoc = new System.Windows.Forms.ListBox();
            this.groupBox_cac_dich_vu = new System.Windows.Forms.GroupBox();
            this.checkBox_su_dung_may_tinh = new System.Windows.Forms.CheckBox();
            this.checkBox_su_dung_thu_vien = new System.Windows.Forms.CheckBox();
            this.button_xem = new System.Windows.Forms.Button();
            this.button_save = new System.Windows.Forms.Button();
            this.button_exit = new System.Windows.Forms.Button();
            this.groupBox_gioi_tinh.SuspendLayout();
            this.groupBox_cac_dich_vu.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label1.Location = new System.Drawing.Point(264, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(283, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Thông Tin Học Viên";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(26, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 29);
            this.label2.TabIndex = 1;
            this.label2.Text = "Họ và tên";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(26, 142);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 29);
            this.label3.TabIndex = 2;
            this.label3.Text = "Số CM";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(26, 217);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 29);
            this.label4.TabIndex = 3;
            this.label4.Text = "Địa chỉ";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(459, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 29);
            this.label5.TabIndex = 4;
            this.label5.Text = "Năm sinh";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(459, 142);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(122, 29);
            this.label6.TabIndex = 5;
            this.label6.Text = "Khóa học";
            // 
            // textBox_ho_ten
            // 
            this.textBox_ho_ten.Location = new System.Drawing.Point(154, 63);
            this.textBox_ho_ten.Multiline = true;
            this.textBox_ho_ten.Name = "textBox_ho_ten";
            this.textBox_ho_ten.Size = new System.Drawing.Size(247, 29);
            this.textBox_ho_ten.TabIndex = 6;
            // 
            // textBox_so_cm
            // 
            this.textBox_so_cm.Location = new System.Drawing.Point(154, 142);
            this.textBox_so_cm.Multiline = true;
            this.textBox_so_cm.Name = "textBox_so_cm";
            this.textBox_so_cm.Size = new System.Drawing.Size(247, 29);
            this.textBox_so_cm.TabIndex = 7;
            // 
            // richTextBox_dia_chi
            // 
            this.richTextBox_dia_chi.Location = new System.Drawing.Point(148, 205);
            this.richTextBox_dia_chi.Name = "richTextBox_dia_chi";
            this.richTextBox_dia_chi.Size = new System.Drawing.Size(253, 122);
            this.richTextBox_dia_chi.TabIndex = 8;
            this.richTextBox_dia_chi.Text = "";
            // 
            // listBox_nam_sinh
            // 
            this.listBox_nam_sinh.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox_nam_sinh.FormattingEnabled = true;
            this.listBox_nam_sinh.ItemHeight = 25;
            this.listBox_nam_sinh.Items.AddRange(new object[] {
            "1970",
            "1971",
            "1972",
            "1973",
            "1974",
            "1975",
            "1976",
            "1977",
            "1978",
            "1979",
            "1980",
            "1981",
            "1982",
            "1983",
            "1984",
            "1985",
            "1986",
            "1987",
            "1988",
            "1989",
            "1990",
            "1991",
            "1992",
            "1993",
            "1994",
            "1995",
            "1996",
            "1997",
            "1998",
            "1999",
            "2000",
            "2001",
            "2002",
            "2003",
            "2004",
            "2005",
            "2006",
            "2007",
            "2008",
            "2009",
            "2010",
            "2011",
            "2012",
            "2013",
            "2014",
            "2015",
            "2016",
            "2017",
            "2018",
            "2019",
            "2020"});
            this.listBox_nam_sinh.Location = new System.Drawing.Point(587, 63);
            this.listBox_nam_sinh.Name = "listBox_nam_sinh";
            this.listBox_nam_sinh.Size = new System.Drawing.Size(201, 29);
            this.listBox_nam_sinh.TabIndex = 9;
            // 
            // comboBox_khoa_hoc
            // 
            this.comboBox_khoa_hoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_khoa_hoc.FormattingEnabled = true;
            this.comboBox_khoa_hoc.Items.AddRange(new object[] {
            "2017 - 2018",
            "2018 - 2019",
            "2019 - 2020"});
            this.comboBox_khoa_hoc.Location = new System.Drawing.Point(587, 138);
            this.comboBox_khoa_hoc.Name = "comboBox_khoa_hoc";
            this.comboBox_khoa_hoc.Size = new System.Drawing.Size(197, 33);
            this.comboBox_khoa_hoc.TabIndex = 10;
            // 
            // groupBox_gioi_tinh
            // 
            this.groupBox_gioi_tinh.Controls.Add(this.radioButton_nu);
            this.groupBox_gioi_tinh.Controls.Add(this.radioButton_nam);
            this.groupBox_gioi_tinh.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_gioi_tinh.Location = new System.Drawing.Point(464, 205);
            this.groupBox_gioi_tinh.Name = "groupBox_gioi_tinh";
            this.groupBox_gioi_tinh.Size = new System.Drawing.Size(310, 121);
            this.groupBox_gioi_tinh.TabIndex = 11;
            this.groupBox_gioi_tinh.TabStop = false;
            this.groupBox_gioi_tinh.Text = "Giới tính";
            // 
            // radioButton_nu
            // 
            this.radioButton_nu.AutoSize = true;
            this.radioButton_nu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_nu.Location = new System.Drawing.Point(194, 55);
            this.radioButton_nu.Name = "radioButton_nu";
            this.radioButton_nu.Size = new System.Drawing.Size(58, 29);
            this.radioButton_nu.TabIndex = 1;
            this.radioButton_nu.TabStop = true;
            this.radioButton_nu.Text = "Nữ";
            this.radioButton_nu.UseVisualStyleBackColor = true;
            // 
            // radioButton_nam
            // 
            this.radioButton_nam.AutoSize = true;
            this.radioButton_nam.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_nam.Location = new System.Drawing.Point(58, 55);
            this.radioButton_nam.Name = "radioButton_nam";
            this.radioButton_nam.Size = new System.Drawing.Size(74, 29);
            this.radioButton_nam.TabIndex = 0;
            this.radioButton_nam.TabStop = true;
            this.radioButton_nam.Text = "Nam";
            this.radioButton_nam.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(26, 374);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(172, 29);
            this.label7.TabIndex = 12;
            this.label7.Text = "Thời gian học";
            // 
            // listBox_thoi_gian_hoc
            // 
            this.listBox_thoi_gian_hoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox_thoi_gian_hoc.FormattingEnabled = true;
            this.listBox_thoi_gian_hoc.ItemHeight = 20;
            this.listBox_thoi_gian_hoc.Items.AddRange(new object[] {
            "Từ 07:00 - đến 09:00",
            "Từ 09:00 - đến 11:00",
            "Từ 13:00 - đến 15:00",
            "Từ 15:00 - đến 17:00",
            "Từ 17:45 - đến 21:00"});
            this.listBox_thoi_gian_hoc.Location = new System.Drawing.Point(205, 374);
            this.listBox_thoi_gian_hoc.Name = "listBox_thoi_gian_hoc";
            this.listBox_thoi_gian_hoc.Size = new System.Drawing.Size(196, 104);
            this.listBox_thoi_gian_hoc.TabIndex = 13;
            // 
            // groupBox_cac_dich_vu
            // 
            this.groupBox_cac_dich_vu.Controls.Add(this.checkBox_su_dung_may_tinh);
            this.groupBox_cac_dich_vu.Controls.Add(this.checkBox_su_dung_thu_vien);
            this.groupBox_cac_dich_vu.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_cac_dich_vu.Location = new System.Drawing.Point(469, 373);
            this.groupBox_cac_dich_vu.Name = "groupBox_cac_dich_vu";
            this.groupBox_cac_dich_vu.Size = new System.Drawing.Size(304, 104);
            this.groupBox_cac_dich_vu.TabIndex = 14;
            this.groupBox_cac_dich_vu.TabStop = false;
            this.groupBox_cac_dich_vu.Text = "Các dịch vụ";
            // 
            // checkBox_su_dung_may_tinh
            // 
            this.checkBox_su_dung_may_tinh.AutoSize = true;
            this.checkBox_su_dung_may_tinh.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_su_dung_may_tinh.Location = new System.Drawing.Point(17, 68);
            this.checkBox_su_dung_may_tinh.Name = "checkBox_su_dung_may_tinh";
            this.checkBox_su_dung_may_tinh.Size = new System.Drawing.Size(186, 29);
            this.checkBox_su_dung_may_tinh.TabIndex = 1;
            this.checkBox_su_dung_may_tinh.Text = "Sử dụng máy tính";
            this.checkBox_su_dung_may_tinh.UseVisualStyleBackColor = true;
            // 
            // checkBox_su_dung_thu_vien
            // 
            this.checkBox_su_dung_thu_vien.AutoSize = true;
            this.checkBox_su_dung_thu_vien.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_su_dung_thu_vien.Location = new System.Drawing.Point(17, 33);
            this.checkBox_su_dung_thu_vien.Name = "checkBox_su_dung_thu_vien";
            this.checkBox_su_dung_thu_vien.Size = new System.Drawing.Size(181, 29);
            this.checkBox_su_dung_thu_vien.TabIndex = 0;
            this.checkBox_su_dung_thu_vien.Text = "Sử dụng thư viện";
            this.checkBox_su_dung_thu_vien.UseVisualStyleBackColor = true;
            // 
            // button_xem
            // 
            this.button_xem.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_xem.Location = new System.Drawing.Point(85, 499);
            this.button_xem.Name = "button_xem";
            this.button_xem.Size = new System.Drawing.Size(113, 44);
            this.button_xem.TabIndex = 15;
            this.button_xem.Text = "Xem";
            this.button_xem.UseVisualStyleBackColor = true;
            // 
            // button_save
            // 
            this.button_save.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_save.Location = new System.Drawing.Point(360, 499);
            this.button_save.Name = "button_save";
            this.button_save.Size = new System.Drawing.Size(113, 44);
            this.button_save.TabIndex = 16;
            this.button_save.Text = "Save";
            this.button_save.UseVisualStyleBackColor = true;
            // 
            // button_exit
            // 
            this.button_exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_exit.Location = new System.Drawing.Point(603, 499);
            this.button_exit.Name = "button_exit";
            this.button_exit.Size = new System.Drawing.Size(113, 44);
            this.button_exit.TabIndex = 17;
            this.button_exit.Text = "Exit";
            this.button_exit.UseVisualStyleBackColor = true;
            this.button_exit.Click += new System.EventHandler(this.button_exit_Click);
            // 
            // ThongTinHocVien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 555);
            this.Controls.Add(this.button_exit);
            this.Controls.Add(this.button_save);
            this.Controls.Add(this.button_xem);
            this.Controls.Add(this.groupBox_cac_dich_vu);
            this.Controls.Add(this.listBox_thoi_gian_hoc);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.groupBox_gioi_tinh);
            this.Controls.Add(this.comboBox_khoa_hoc);
            this.Controls.Add(this.listBox_nam_sinh);
            this.Controls.Add(this.richTextBox_dia_chi);
            this.Controls.Add(this.textBox_so_cm);
            this.Controls.Add(this.textBox_ho_ten);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ThongTinHocVien";
            this.Text = "Thông Tin Học Viên";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ThongTinHocVien_FormClosing);
            this.groupBox_gioi_tinh.ResumeLayout(false);
            this.groupBox_gioi_tinh.PerformLayout();
            this.groupBox_cac_dich_vu.ResumeLayout(false);
            this.groupBox_cac_dich_vu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_ho_ten;
        private System.Windows.Forms.TextBox textBox_so_cm;
        private System.Windows.Forms.RichTextBox richTextBox_dia_chi;
        private System.Windows.Forms.ListBox listBox_nam_sinh;
        private System.Windows.Forms.ComboBox comboBox_khoa_hoc;
        private System.Windows.Forms.GroupBox groupBox_gioi_tinh;
        private System.Windows.Forms.RadioButton radioButton_nu;
        private System.Windows.Forms.RadioButton radioButton_nam;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListBox listBox_thoi_gian_hoc;
        private System.Windows.Forms.GroupBox groupBox_cac_dich_vu;
        private System.Windows.Forms.CheckBox checkBox_su_dung_may_tinh;
        private System.Windows.Forms.CheckBox checkBox_su_dung_thu_vien;
        private System.Windows.Forms.Button button_xem;
        private System.Windows.Forms.Button button_save;
        private System.Windows.Forms.Button button_exit;
    }
}

